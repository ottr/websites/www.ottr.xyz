
   - [[https://ontocommons.eu/news-events/events/workshop-ontology-engineering-and-knowledge-graphs-tool-ecosystem][OntoCommons Workshop on Ontology Engineering and Knowledge Graphs tool ecosystem]] 2022-08-31, /Reasonable Ontology Templates/, Martin G. Skjæveland.

   - [[./event/2022-05-11-user-forum/][3rd OTTR user forum]] 2022-05-11, OTTR team + invited talks.

   - UiO/SIRIUS general assembly, /Ontology Engineering Research
     Program + Reasonable Ontology Templates/, Martin G. Skjæveland.

   - [[./event/2021-06-18-user-forum/][2nd OTTR user forum]] 2021-06-18, OTTR team + invited talks.

   - [[./event/2021-01-28-user-forum/][1st OTTR user forum]] 2021-01-28, OTTR team + invited talks.

   - Presentation for Strategic Advisory Committee at the Faculty of
     Mathematics and Natural Sciences UiO 2019-11-28, /OTTR Templates/,
     Martin G. Skjæveland.

   - OSDU workshop 2019-10-08, /OTTR: Scalable construction of sustainable knowledge bases/, Martin G. Skjæveland and Dag Hovland.

   - UiO/SIRIUS General Assembly 2019-05-23, /Ontology engineering for industrial applications using Reasonable Ontology Templates(OTTR)/, Martin G. Skjæveland.

   - CapGemini semantics workshop 2019-04-26, /Reasonable Ontology
     Templates/, Martin G. Skjæveland.
   
   - ISWC 2018 research track: [[./event/2018-10-08-iswc/iswc2018-research.pdf][Reasonable Ontology Templates]], Martin G. Skjæveland.

   - ISWC 2018 industry track: [[./event/2018-10-08-iswc/iswc2018-industry.pdf][Semantic Material Master Data
     Management at Aibel]], Anders Gjerver (Aibel) and
     Martin G. Skjæveland.

   - UiO/SIRIUS Seminar 2018-08-27, /Reasonable Ontology Templates/,
     Martin G. Skjæveland, Leif Harald Karlsen and Daniel Lupp.

   - UiO/SIRIUS workshop with University of Western Australia
     2018-08-15, /Reasonable Ontology Templates/, Martin
     G. Skjæveland, Leif Harald Karlsen and Daniel Lupp.

   - Bielefeld University visit 2018-06-05, /Reasonable Ontology
     Templates/, Martin G. Skjæveland, Leif Harald Karlsen and Daniel
     Lupp.

   - Uni Bremen visit 2018-06-04, /Reasonable Ontology Templates/,
     Martin G. Skjæveland and Leif Harald Karlsen.

   - [[https://www.meetup.com/SIRIUS-Industrial-Ontology-Colloquium/events/249630274/][SIRIUS' Industrial Ontology Colloquium]] 2018-04-13: [[https://www.slideshare.net/MartinGSkjveland/programming-semantics-practical-applications-of-ottr][Programming semantics:
     practical applications of OTTR]], Leif Harald Karlsen.

   - TU Wien visit 2017-11-30, /Reasonable Ontology Templates---When macros are more than just macros/, Daniel Lupp.

   - [[http://ontologydesignpatterns.org/wiki/WOP:2017][Workshop on Ontology Design and Patterns 2017]]: [[./event/2017-10-21-wop/wop2017.pdf][Reasonable
     Ontology Templates]], Martin G. Skjæveland.

   - SIRIUS' Industrial Ontology Colloquium 2017-09-29: /Reasonable Ontology
    Templates/, Martin G. Skjæveland.

   - SIRIUS' Industrial Ontology Colloquium 2017-03-03: /Ontology
    Templates/, Martin G. Skjæveland.

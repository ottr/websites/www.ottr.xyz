
// Build TOC in left margin

$(document).ready(function() {
    
    var contentID = "toc-content";
    var tocID = "toc";
    var toc = "";
    var level = 1;
    var maxDepth = 4; // no toc for headings of h5 or less
    var list = "ul";

    document.getElementById(contentID).innerHTML =
        document.getElementById(contentID).innerHTML.replace(
                /<h([\d])([^>]*)>(.+?)<\/h([\d])>/gi,
	    function (str, openLevel, headingAttr, titleText, closeLevel) {
		if (openLevel != closeLevel) {
		    return str;
		}

		if (openLevel > level) {
		    toc += (new Array(openLevel - level + 1)).join("<" + list + ">");
		} else if (openLevel < level) {
		    toc += (new Array(level - openLevel + 1)).join("</" + list + ">");
		}

		level = parseInt(openLevel);

		var anchor = titleText.replace(/<[^<]+>/g, "").replace(/ /g, "_");

		if (level < maxDepth && !titleText.endsWith('_footer')) {
		    toc += "<li><a href=\"#" + anchor + "\" " + headingAttr + ">" + titleText  + "</a></li>";
		}

		return "<h" + openLevel + " " + headingAttr + ">"
                    + "<a name=\"" + anchor + "\">" + titleText + "</a>"
                    + "<a class=\"anchormark\" href=\"#" + anchor + "\">&#128279;</a>"
                    + "</h" + closeLevel + ">";
	    }
	);

    if (level) {
	toc += (new Array(level + 1)).join("</" + list + ">");
    }

    document.getElementById(tocID).innerHTML += toc;
    
});

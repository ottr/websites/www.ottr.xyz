$(document).ready(function() {

    var scrollDiv = document.getElementById("right");
    var isVisibleDiv = document.getElementById("toc-content");
    var showDiv = document.getElementById("left");

    var whenVisible = function () {
	$(isVisibleDiv).fadeTo("slow", 1, function() {});
	if (showDiv) {
	    $(showDiv).fadeTo("slow", 1, function() {});
	}
    }

    var isVisible = false;

    scrollDiv.onscroll = function () {

	var scroll = $(scrollDiv).scrollTop() + 100;
	var vis = $(isVisibleDiv).offset().top;
	
	if (!isVisible && scroll >= vis) {
	    whenVisible();
	    isVisible = true;
	}
	// else {
	    // comment out fading out:
	    //$(showDiv).fadeOut();
	//}
    };
});

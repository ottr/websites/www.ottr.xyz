#+OPTIONS: html-link-use-abs-url:nil html-postamble:nil html-preamble:nil html-scripts:nil html-style:nil html5-fancy:t
#+OPTIONS:  tex:t toc:nil num:t H:4
#+HTML_DOCTYPE: xhtml5
#+HTML_CONTAINER: div
#+DESCRIPTION:
#+KEYWORDS:
#+TITLE: 1st OTTR user forum, 2021-01-28
#+HTML_LINK_HOME:
#+HTML_LINK_UP:
#+HTML_MATHJAX:
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="https://www.ottr.xyz/inc/style.css" />
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="https://www.ottr.xyz/inc/spec.css" />
#+HTML_HEAD: <script type="application/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
#+HTML_HEAD: <script type="application/javascript" src="https://www.ottr.xyz/inc/toc.js"></script>
#+HTML_HEAD_EXTRA:
#+SUBTITLE:
#+INFOJS_OPT:
#+LATEX_HEADER:

#+BEGIN_EXPORT html
<div id="left">
<p>
  <a href="#top">top</a>
  <!--<br/>
  examples:
  <span class="button" onclick="$('.examplebox').show()">show</span>
  /
  <span class="button" onclick="$('.examplebox').hide()">hide</span>
  -->
</p>
  <h4>Table of Contents</h4>
  <div id="toc"></div>
</div>
#+END_EXPORT

#+BEGIN_EXPORT html
<div id="right">
  <a name="top"></a>
  <h1 class="title">1st OTTR user forum, 2021-01-28</h1>
  <img id="logo" alt="OTTR" src="../../logo/OTTR.jpg"/>
#+END_EXPORT

#+HTML: <div id="toc-content">

The 1st OTTR user forum will take place virtually on *2021-01-28 Thu 12:00--14:00 CET*. The purpose of the event is to let users of the OTTR
framework share experiences and to meet the developers behind OTTR to
discuss future directions. The event should also be of interest to
those that are curious to see practical uses of the OTTR framework.
We also welcome input and presentations of tools and technologies that
are related to OTTR.

To register for the event, please fill in the following form:
++https://forms.gle/PH7i9DSd4d6nx3nP7++ (Deadline Jan 12 or Jan 24, see
below) Connection details will be sent in the week of the event to
those who have registered.


* Agenda

|         When | Who                                                 | What                                                                          |
|--------------+-----------------------------------------------------+-------------------------------------------------------------------------------|
| 12:00--12:30 | OTTR team                                           | Welcome, project presenation, current status, future plans                    |
| 12:30--12:40 | All                                                 | Discusssion                                                                   |
| 12:40--12:50 | All                                                 | Break                                                                         |
| 12:50--13:35 | Johan W. Klüwer, DNVGL                              | [[Invited talk][Invited talk]]: /READI Joint industry project/. 35 + 10 mins. [[https://vimeo.com/510594172][Video]] [[./slides-johan.pdf][Slides]]      |
| 13:35--13:50 | Melinda Hodkiewicz, University of Western Australia | Talk: /Pre-processing unstructured text for OTTR/. 10 + 5 mins. [[https://vimeo.com/510603136][Video]]  [[./slides-melinda.pdf][Slides]] |
| 13:50--14:00 | OTTR team, all                                      | Wrap up                                                                       |

* Invited talk

Johan W. Klüwer, DNV-GL will give an invited talk and demonstration.
[[./slides-johan.pdf][Slides]].

Abstract: We present work done in the READI Joint industry project
(https://readi-jip.org/), demonstrating how OTTR can be used to
interpret existing industrial vocabularies as OWL. The main part will
be a walk-through of the process: starting with tabular data, adding
structure with SQL views, designing templates with stOTTR, and mapping
in bOTTR declarations. We comment briefly on the importance of
applying an upper ontology; here, ISO 15926-14. The result is an OWL
ontology that considerably enriches the tabular-format data and is
ready for dissemination as Linked Data. The use case will be the
CFIHOS RDL and Data dictionary, downloadable from
https://www.jip36-cfihos.org/cfihos-standards/.

Bio: Johan W. Klüwer is a specialist in industrial ontologies at DNV
GL, Oslo. He has contributed to ontology development and
implementation for capital projects, standards as ontologies, research
and development projects, and international collaboration. He is a
primary contributor to the methodology of the ongoing READI Joint
Industry Project (2018-) that targets digitalisation of requirements
for the Oil and Gas on the Norwegian Continental Shelf.

#+HTML: <iframe style="border: 1px solid #ccc;" src="https://player.vimeo.com/video/510594172" width="640" height="360"  allow="autoplay; fullscreen" allowfullscreen></iframe>

* Talk

Melinda Hodkiewicz, Professor, University of Western Australia.
[[./slides-melinda.pdf][Slides]].

Abstract: What to do when the data you want to ingest using OTTR is
unstructured text? This talk looks at work to annotate entity types in
maintenance data sets to make the instance data amenable to
ingestion. We suggest there is a need to consider the ontological
needs a priori in the selection of entities and annotation stages.

#+HTML: <iframe style="border: 1px solid #ccc;" src="https://player.vimeo.com/video/510603136" width="640" height="360"  allow="autoplay; fullscreen" allowfullscreen></iframe>

* Registration
The event is open and free. To register for the event,
please fill in the following form: https://forms.gle/PH7i9DSd4d6nx3nP7

If you want to present, the deadline for registering is Jan 12. You
are then required to provide a title and abstract for your
presentation which will be used in the announcement of the event.

If you do not want to present, the deadline for registering is Jan 24.

The agenda for the user forum, including connection details, will be
distributed on Jan 25 to those that have registered.

Hope to see you there!

* _footer                                                          :noexport:
#+HTML: </div> <!-- end toc-content -->
#+HTML: </div> <!-- end right -->

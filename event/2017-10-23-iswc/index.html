<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Reasonable Ontology Templates (OTTR)</title>
    <link rel="stylesheet" type="text/css" href="../../inc/style.css" />
    <link rel="stylesheet" type="text/css" href="../../inc/spec.css" />
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/atom-one-light.min.css">
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" async src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.2/MathJax.js?config=TeX-AMS_CHTML"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>
    <script>hljs.initHighlightingOnLoad();</script>

    <style type="text/css">
      i.def { background-color: lightyellow; }
      span.button { text-decoration: underline; cursor: pointer; }
      span.button:hover { background-color: lightyellow; }
      p.abstract { max-width: 650px; }
      object.code { width: 900px; }
      pre.code { background-color: transparent; margin: 0; padding: 3px; border: 1px solid #aaa;}
      .hljs { background-color: transparent; }
    </style>      

    
    <script type="application/javascript" src="../../inc/toc.js"></script>

    <script type="text/x-mathjax-config">
      MathJax.Hub.Config({
      tex2jax: {inlineMath: [['$','$'],['\\(','\\)']]},
      styles: {
      ".MathJax_Display": {
      "text-align": "left",
      margin:       "1em 0em"
      }
      },
      TeX: {
      TagSide: "left",
      Macros: {
      map: ['::',0],
      mc: ['\\mathcal{#1}',1],
      tname: ['\\color{blue}{\\mathsf{#1}}',1],
      tpara: ['\\color{green}{#1}',1],
      ont: ['\\color{brown}{#1}',1],
      boxt: ['\\mathcal{O}_{\\mathcal{T}}', 0]
      }
      }
      });
    </script>

    <script>
          $(document).ready(function() {
	  $("#ex-rdf-partof2").hide();
	  });
    </script>

  </head>
  <body>
    
    <div id="left">
      <p>
      <a href="#top">top</a><br/>
      toggle: 
      <span class="button" onclick="$('.examplebox').fadeToggle()">examples</span> - 
      <span class="button" onclick="$('.note').fadeToggle()">notes</span>
      </p>
      <h4>Table of Contents</h4>
      <div id="toc"></div>
    </div>


    <div id="right">
      <a name="top"/>
      <h1 class="title">Reasonable Ontology Templates (OTTR)</h1>
      <img id="logo" alt="OTTR" src="../../logo/demOTTR.png"/>
      <p><b>Companion text and demo for ISWC 2017 poster presentation.</b></p>

      <p class="notice">The concepts used in this demo are still valid,
	  however, tools and formats have been updated. Please
	  see <a href="http://ottr.xyz">ottr.xyz</a>.
      </p>


      <h3>Abstract</h3>

      <p class="abstract">
	Reasonable Ontology Templates, OTTRs for short, are OWL ontology
	macros capable of representing ontology design patterns (ODPs) and
	closely integrating their use into ontology engineering. An OTTR is
	itself an OWL ontology or RDF graph, annotated with a special purpose
	OWL vocabulary. This allows OTTRs to be edited, debugged, published,
	identified, instantiated, combined, used as queries and bulk
	transformations, and maintained—all leveraging existing W3C standards,
	best practices and tools. We show how such templates can drive a
	technical framework and tools for a practical, efficient and
	transparent use of ontology design patterns in ontology design and
	instantiation.
      </p>

      <h3>Resources</h3>
      <ul>
	<li>Poster paper: <a href="https://iswc2017.semanticweb.org/wp-content/uploads/papers/PostersDemos/paper597.pdf">Reasonable Ontology Templates: APIs for OWL</a></li>
	<li><a href="poster.pdf">Poster</a></li>
	<li>WOP2017 paper: <a href="http://ontologydesignpatterns.org/wiki/images/6/66/Paper-04.pdf">Pattern-Based Ontology Design and Instantiation with Reasonable Ontology Templates</a></li>
	<li>OTTR home: <a href="http://www.ottr.xyz">ottr.xyz</a></li>
	
	<li>OTTR Templates library: <a href="http://library.ottr.xyz">library.ottr.xyz</a></li>
	<li> <a href="http://www.ottr.xyz#implementation">CLI implementation</a></li>
	<li>git repo: <a href="http://gitlab.com/ottr">http://gitlab.com/ottr</a></li>
      </ul>


      <!-- Headings in this container gets into the TOC -->
      <div id="toc-content">
	
	<h2>1. Ontology Templates</h2>

	<p>
	  The formal definition of ontology templates is based on
	  description logics, while the implemented version is based
	  on RDF graphs.
	</p>

	<h3>1.1 Description Logic Formalisation</h3>

	<p>
	  An <i class="def">ontology template</i> $\mathcal T$ is a
	  knowledge base $\boxt$ and a list of parameters
	  $(\tpara{p_1},\ldots,\tpara{p_n})$ of distinguished concept,
	  role, or individual names from $\boxt$. We write a template
	  as

	  \[ \tname{\mc{T}}(\tpara{p_1},\ldots,\tpara{p_n}):: \boxt.
	  \]

	  and refer to the left side as the <i class="def">head</i>
	  and the right side as the <i class="def">body</i>.
	</p>

	<p>
	  For a list of parameters $(q_1,\ldots,q_n)$ 
	  we call $\tname{\mathcal T}(q_1,\ldots,q_n)$ a
	  <i class="def">template instance</i>. Intuitively, a
	  template instance is shorthand for representing a specific
	  occurrence or instance of a pattern. More precisely,
	  the <i class="def">expansion</i> of $\tname{\mathcal
	  T}(q_1,\ldots,q_n)$ is the ontology $\boxt(q_1,\ldots,q_n)$
	  obtained by replacing each parameter occurrence of
	  $\tpara{p_i}$ in $\boxt$ with the argument $q_i$, for all
	  $1\leq i \leq n$.
	</p>


	<div class="examplebox">
	  <h4 class="example-title">Simple template (DL)</h4>
	  <p>
	    This is a template:

	    $$
	    \tname{PartOf}(\tpara{part}, \tpara{whole})
	    \map
	    \{ \tpara{whole} \sqsubseteq \exists \ont{hasPart} . \tpara{part} \}
	    $$

	    The name of the template is $\tname{PartOf}$. It has a
	    single axiom knowledge base $\{\tpara{whole} \sqsubseteq
	    \exists \ont{hasPart} . \tpara{part}\}$ where
	    $\ont{hasPart}$ is a role name and $\tpara{part}$ and
	    $\tpara{whole}$ are parameters.
	    </p>
	</div>
	
	<div class="examplebox">
	  <h4 class="example-title">Simple instance template (DL)</h4>
	  <p>
	    This is an instance of the $\tname{PartOf}$ template:

	    $$\tname{PartOf}(\ont{SteeringWheel}, \ont{Car})$$

	    The instance expands to the ontology $\{ \ont{Car} \sqsubseteq
	    \exists \ont{hasPart} . \ont{SteeringWheel} \}$.

	    <br/><br/>
	    Note
	    that also $\tname{PartOf}(\tpara{part}, \tpara{whole})$ is
	    an instance of $\tname{PartOf}$, where the parameter names
	    are substituted for themselves; its ontology is $\{
	    \tpara{whole} \sqsubseteq \exists \ont{hasPart}
	    . \tpara{part} \}$.
	    </p>
	</div>


	<p>
	  Templates are modular:
	  in addition to ontology axioms, a template may also contain
	  template instances in its body. The notion of template
	  instance expansion is then extended to a recursive operation
	  where any template instances in the template body are
	  expanded tail-recursively. Cyclic template definitions are
	  not allowed.
	</p>

	<div class="examplebox" name="simple template">
	  <h4 class="example-title">Complex template (DL)</h4>
	  <p>
	Let $\tname{QualityValue}$ be the template 

	  \begin{align*}
	  &\tname{QualityValue}(\tpara{x}, \tpara{hasQuality}, \tpara{uom}, \tpara{val}) \map \\
	  &\quad\{\tpara{x} \sqsubseteq \exists \tpara{hasQuality} .(\forall \ont{hasDatum} .(\exists \ont{hasUOM}
	  . \{\tpara{uom}\} \sqcap \exists \ont{hasValue} . \{\tpara{val}\}))\}
	  \end{align*}

	  which intuitively expresses that $\tpara{x}$ has a quality
	  with a given value $\tpara{val}$ with a given unit of
	  measurement $\tpara{uom}$.
	  <br><br>

	  Using this template and the $\tname{PartOf}$ template, and
	  fixing some of the parameters, the template
	  $\tname{PartLength}$ can be defined as

	  \begin{align*}
	  \tname{PartLength}(\tpara{whole}, \tpara{part}, \tpara{length})
	  \map \{
	  &\tname{PartOf}(\tpara{part}, \tpara{whole}),\\
	  &\tname{QualityValue}(\tpara{part}, \ont{hasLength}, \ont{meter}, \tpara{length})\}
	  \end{align*}
	  
	  which expresses that the whole has a part with a given length measured in meters.
	  <br><br>

	  An example instance of the template is
	  $\tname{PartLength}(\ont{2CV}, \ont{SoftTop}, \ont{1.40})$
	  stating that (the car) 2CV has a softtop (roof) of length
	  1.40 meters.

	  The expansion of the instance is

	  \begin{align*}
	  \{ \ont{2CV} &\sqsubseteq \exists \ont{hasPart} . \ont{SoftTop},\\
	  \phantom{\{}\ont{SoftTop} &\sqsubseteq \exists \ont{hasLength} .(\forall \ont{hasDatum} . 
	  (\exists \ont{hasUOM}
	  . \{\ont{meter}\} \sqcap \exists \ont{hasValue} . \{\ont{1.40}\}))\}\text{.}
	  \end{align*}
	  </p>
	</div>


	<h3>1.2 OWL/RDF formalisation</h3>

	<p>
	Ontology templates are adopted to the semantic web by
	serialising them using RDF with
	the <a href="http://ns.ottr.xyz">OTTR OWL vocabulary</a>
	defined for this task.
	</p>
	<p>
	A template is associated with an RDF graph (document)
	available at the IRI of the template.  (Similarly as the recommended practice for <a href="https://www.w3.org/TR/owl2-syntax/#Ontology_Documents">OWL
	ontology documents</a>.)
	The RDF graph contains both the head, identifying the template
	and its parameters, and the body of the template. The template
	body may contain template instances and other ontology axioms,
	which are expressed using regular RDF/OWL
	serialisation.


	We differentiate between the head and the body of a template
	represented in RDF using the concept of outgoing graph
	neighbourhood, which informally are all the outgoing triples
	from the template and parameter individuals.
	</p>

	<p>
	Parameters and arguments are represented as named variables
	and named values, respectively, where the name is represented
	in RDF as an IRI, and variables and values may be any RDF
	resource, i.e., any IRI or literal.
	</p>

	<div class="block note">
	A Template RDF graph need not represent an OWL
	ontology.  In fact, templates may be used in a more generic
	way as "RDF macros". However, we prefer to call them OWL
	macros in order to clearly indicate their applicability to
	ontology engineering, reasoning and ontology design patterns.
	</div>

	<div class="examplebox">
	  <h4 class="example-title">Simple template 1</h4>
	  <p>
	  The $\tname{PartOf}$ template in OTTR serialisation, shown
	  in two different variants: one specifying its parameter
	  variables using indexed parameters and the other using an
	  RDF list of parameter variables
<span class="button" onclick="$('#ex-rdf-partof1').fadeToggle();$('#ex-rdf-partof2').fadeToggle();">(click to show/hide)</span>.
	  </p>
	<pre class="code"><code>@prefix ottr:    &lt;http://ns.ottr.xyz/templates#&gt; .
@prefix partOf:  &lt;http://www.ontologydesignpatterns.org/cp/owl/partof.owl#&gt; .
@prefix :        &lt;http://draft.ottr.xyz/i17/partof#&gt; .

  ### head:
&lt;http://draft.ottr.xyz/i17/partof&gt; a ottr:Template ;<b id="ex-rdf-partof1">
    ottr:hasParameter [ ottr:index 1; ottr:variable :Whole ] , # indexed parameters
                      [ ottr:index 2; ottr:variable :Part  ] .  </b><b id="ex-rdf-partof2">
    ottr:withVariables ( :Whole  :Part ) .  ## variable list
</b>
  ### body:
:Part  a owl:Class .
:Whole a owl:Class ; 
    rdfs:subClassOf [ a owl:Restriction ; 
        owl:onProperty partOf:hasPart ; owl:someValuesFrom :Part ] .</code></pre>

	<p>
	This template is published at the IRI of the template
	<a href="http://draft.ottr.xyz/i17/partof"><code>http://draft.ottr.xyz/i17/partof</code></a>
	and can be viewed in the online ontology template library
	<a href="http://osl.ottr.xyz/info/?tpl=http://draft.ottr.xyz/i17/partof"><code>
	    http://osl.ottr.xyz/info/?tpl=http://draft.ottr.xyz/i17/partof</code></a>.</p>
	</div>

	
	
		<div class="examplebox">
	  <h4 class="example-title">Simple template 2</h4>
	  <p>
	    The $\tname{PartOf}$ template in OTTR serialisation now
	    defined using a instance of
	    the <code>ottr-owl:SubObjectSomeValuesFrom</code>
	    template in the body, which expresses the
	    existential qualified axiom.
	  </p>
	<pre class="code"><code>@prefix ottr:    &lt;http://ns.ottr.xyz/templates#&gt; .
@prefix partOf:  &lt;http://www.ontologydesignpatterns.org/cp/owl/partof.owl#&gt; .
@prefix ottr-owl:  &lt;http://candidate.ottr.xyz/owl/axiom/&gt; .
@prefix :        &lt;http://draft.ottr.xyz/i17/partof#&gt; .

  ### head:
&lt;http://draft.ottr.xyz/i17/partof&gt; a ottr:Template ;
    ottr:withVariables ( :Whole  :Part )

  ### body:
[] ottr:templateRef ottr-owl:SubObjectSomeValuesFrom ; 
   ottr:withValues ( :Whole partOf:hasPart :Part ) .	    
</code></pre>

	<p>
	  <ul>
	    <li>
	<a href="http://draft.ottr.xyz/i18/partof"><code>http://draft.ottr.xyz/i18/partof</code></a></li>
	    <li>
	<a href="http://osl.ottr.xyz/info/?tpl=http://draft.ottr.xyz/i18/partof"><code>
	    http://osl.ottr.xyz/info/?tpl=http://draft.ottr.xyz/i18/partof</code></a></li>
	    </ul>
	</div>

		<h2>2. OTTR library</h2>
	<p>The OTTR library at <a href="http://library.ottr.xyz/">http://library.ottr.xyz</a>
	  contains a set of templates that are available for use. The purpose of the library is
	  to have a publicly available repository of templates that may be actively used in the
	  construction of ontologies.
	</p>
	<p>
	  The library is backed by different git repositories that
	  contain OTTR templates of different status and maturity:
	  candidate, draft, and test. Test contains test suite
	  templates to test the implementation of the OTTR library;
	  draft is a sandbox area where anyone may contribute by
	  pushing to the git repository; candidate contains templates
	  which are relatively stable and are candidates for a
	  standardised version. The formal relationships between these
	  statuses and the management rules of the repositories is not
	  yet established.
	</p>
		
	
	<h2>3. Extensible Framework</h2>

	<p>
	  The implicit relationship between the head and the body of a
	  template and be exploited to generate formats that represent
	  the template head and the template body, and transformations
	  between these formats, like XSD, XSLT, SPARQL SELECT, SPARQL
	  CONSTRUCT and SPARQL UPDATE. We intend to add support for
	  more formats, e.g,. graph validation formats such as SHACL
	  and ShEx. The different formats are available from the web
	  servlet explained below.
	</p>

	<h2>4. OTTR web servlet</h2>

	<p>The web servlet located at <code>http://osl.ottr.xyz</code>
	serves all the different formats that can be genereted from an
	  OTTR template, these can be found in the top right menu on each "information" page of a template, e.g.,
	<a href="http://osl.ottr.xyz/info/?tpl=http://draft.ottr.xyz/i18/partof"><code>
	    http://osl.ottr.xyz/info/?tpl=http://draft.ottr.xyz/i18/partof</code></a>.
	</p>
      </div>
    </div>
  </body>
</html>
